import React, {Component} from 'react';

class Home extends Component {
    render() {
        return (
            <main>
                <div class="cc-navBarContainer">
            	   <img class="cc-navBar" src="orange_nav_bar.png" />
                   <img class="cc-logo" src="logo.png" />
                </div>
                <div class="cc-content">
                    <a className="cc-loginLink" href={FlowRouter.url('login')}>Login</a>
                    <a href={FlowRouter.url('register')}>Register</a>
                </div>
            </main>
        )
    }
}

Home.propTypes = {};
Home.defaultProps = {};

export default Home;
