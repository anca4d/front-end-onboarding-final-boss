import React from 'react';
import {AutoForm, AutoField, ErrorField} from 'uniforms-unstyled';
import DonutsSchema from '/imports/db/donuts/schema';

export default class DonutsCreate extends React.Component {
    constructor() {
        super();
    }

    onSubmit = (data) => {
        Meteor.call('donut.create', data, (err) => {
            if(!err) {
                FlowRouter.go('donuts.list');
            }
        });
    };

    render() {
        return (
            <main>
             <div class="cc-navBarContainer">
                   <img class="cc-navBar" src="../orange_nav_bar.png" />
                   <img class="cc-logo" src="../logo.png" />
                </div>
                 <div class="cc-content">
                    <div class="cc-backgroundWrapper">
                        <AutoForm label={false} placeholder={true} schema={DonutsSchema} onSubmit={this.onSubmit}>
                            <h1 className="cc-title">Add a donut</h1>

                            <div className="cc-fieldWrapper">
                                <AutoField className="cc-field" name="name" />
                            </div>
                            <ErrorField className="cc-errorField" name="name"/>

                            <div className="cc-fieldWrapper">
                                <AutoField className="cc-field" name="price" />
                            </div>
                            <ErrorField className="cc-errorField" name="price"/>

                            <AutoField name="isComestible" label="Is comestible"/>
                            <ErrorField className="cc-errorField" name="isComestible"/>

                            <div className="cc-createDonutWrapper">
                                <button className="cc-createDonut" type="submit">
                                    Create
                                </button>
                            </div>
                        </AutoForm>
                    </div>
                </div>
            </main>
        )
    }
}