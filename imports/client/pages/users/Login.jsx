import React from 'react';
import {AutoForm, AutoField, ErrorField} from 'uniforms-unstyled';
import SimpleSchema from 'simpl-schema';

class Login extends React.Component {
    constructor() {
        super();
    }

    onSubmit = (data) => {
        const {email, password} = data;

        Meteor.loginWithPassword(email, password, (err) => {
            if (!err) {
                FlowRouter.go('donuts');
            } else {
                alert(err.reason);
            }
        });
    };

    render() {
        return (
            <main className="cc-main">
                <div class="cc-navBarContainer">
                   <img class="cc-navBar" src="orange_nav_bar.png" />
                   <img class="cc-logo" src="logo.png" />
                </div>

                <div class="cc-content">
                    <AutoForm label={false} placeholder={true} schema={LoginSchema} onSubmit={this.onSubmit}>
                        <div className="cc-fieldWrapper">
                             <AutoField className="cc-field" name="email" />
                        </div>
                        <ErrorField name="email"/>

                        <div className="cc-fieldWrapper">
                            <AutoField className="cc-field" name="password" type="password" />
                        </div>
                        <ErrorField name="password"/>

                        <div className="cc-submitWrapper">
                            <div class="cc-forgotText">Forgot your password?</div>

                            <button className="cc-submit" type="submit">
                                Sign in
                            </button>
                      
                        </div>
                    </AutoForm>
                </div>
            </main>
        )
    }
}

const LoginSchema = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        // label: null
    },
    password: {
        type: String,
        // label: null
    }
});

export default Login;