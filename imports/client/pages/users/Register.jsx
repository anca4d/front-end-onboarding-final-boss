import React from 'react';
import {AutoForm, AutoField, ErrorField} from 'uniforms-unstyled';
import SimpleSchema from 'simpl-schema';

class Register extends React.Component {
    constructor() {
        super();

    }

    onSubmit = (data) => {
        const {email, password} = data;

        Accounts.createUser({
            email,
            password,
        }, (err) => {
            if (!err) {
                FlowRouter.go('donuts.list');
            }
            else {
                alert(err.reason);
            }
        })
    };

    render() {
        return (
            <main>
                <div class="cc-navBarContainer">
                   <img class="cc-navBar" src="orange_nav_bar.png" />
                   <img class="cc-logo" src="logo.png" />
                </div>

                <div class="cc-content">
                    <AutoForm label={false} placeholder={true} schema={RegisterSchema} onSubmit={this.onSubmit}>
                        <div className="cc-fieldWrapper">
                            <AutoField className="cc-field" name="name"/>
                        </div>
                        <ErrorField name="name"/>

                        <div className="cc-fieldWrapper">
                            <AutoField className="cc-field" name="email"/>
                        </div>
                        <ErrorField name="email"/>

                        <div className="cc-fieldWrapper">
                            <AutoField className="cc-field" name="password" type="password"/>
                        </div>
                        <ErrorField name="password"/>

                        <div className="cc-fieldWrapper">
                        <AutoField className="cc-field" name="confirm_password" type="password"/>
                        </div>
                        <ErrorField name="confirm_password"/>

                        <div className="cc-submitRegisterWrapper">
                            <button className="cc-signup" type="submit">
                                Register
                            </button>
                        </div>
                    </AutoForm>
                </div>
            </main>
        )
    }
}

const RegisterSchema = new SimpleSchema({
    name: {
        type: String,
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    password: {type: String},
    confirm_password: {
        type: String,
        custom() {
            if (this.value !== this.field('password').value) {
                return 'passwordMismatch';
            }
        }
    }
});

export default Register;